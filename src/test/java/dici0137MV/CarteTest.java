package dici0137MV;
import org.junit.*;
import static org.junit.Assert.*;

public class CarteTest {

    private static Carte c1;
    private static Carte c2;

    @BeforeClass
    public static void setUp() {
        c1 = new Carte(1,198, "Povesti");
        c2 = new Carte(2,170,"Povestiri");
    }

    @After
    public void tearDown() {
        c2 = null;
    }

    @Test
    public void getCod() {
        assertEquals(2,c2.getCod());
    }

    @Test
    public void setCod() {
    }

    @Test
    public void getNrPagini() {
        assertEquals(1,c1.getCod());
    }

    @Test
    public void setNrPagini() {
    }

    @Test
    public void getTitlu() {
        assertEquals("Povesti",c1.getTitlu());
    }

    @Test
    public void setTitlu() {
    }

    @Test
    public void testToString() {
    }

    @Test
    public void testConstructor() {
        Carte c3 = new Carte(10,22,"Test1");
        assertNotEquals(c3,null);
    }
}