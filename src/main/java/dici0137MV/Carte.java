package dici0137MV;

public class Carte {

    private int cod, nrPagini;
    private String titlu;

    public Carte(int cod, int nrPagini, String titlu) {
        this.cod = cod;
        this.nrPagini = nrPagini;
        this.titlu = titlu;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public int getNrPagini() {
        return nrPagini;
    }

    public void setNrPagini(int nrPagini) {
        this.nrPagini = nrPagini;
    }

    public String getTitlu() {
        return titlu;
    }

    public void setTitlu(String titlu) {
        this.titlu = titlu;
    }

    @Override
    public String toString() {
        return "Carte{" +
                "cod=" + cod +
                ", nrPagini=" + nrPagini +
                ", titlu='" + titlu + '\'' +
                '}';
    }
}
